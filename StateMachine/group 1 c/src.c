#include <stdio.h>
#include <inttypes.h>

int test_function(int,int);
int main ()
{
	int event,x,state=1;
	while(1)
	{
		printf("State is S%d\n",state);
		printf("Enter the event: \n");
		x=scanf("%d",&event);
		if(!x)
		{
			printf("Invalid Input!\n");
			return 0;
		}
		if(event<1 || event>10)
		{
			printf("Event out of range!\n");
			continue;
		}
		printf("Event: %d\n",event);
		state=test_function(state,event);
	}
	return 0;
}

int test_function (int s, int e)
{
	switch (s) {
		case 1:
			switch (e) {
				case 7:
					return	5;
				case 8:
					return 4;
				default:
					printf("Event is undefined!\n");
					return s;
			}
		case 2:
			switch (e) {
				case 2:
					return 1;
				case 10:
					return 3;
				default:
					printf("Event is undefined!\n");
					return s;
			}
		case 3:
			switch (e) {
				case 1:
					return 2;
				case 9:
					return 3;
				default:
					printf("Event is undefined!\n");
					return s;
			}
		case 4:
			switch (e) {
				case 3:
					return 2;
				case 5:
					return 5;
				default:
					printf("Event is undefined!\n");
					return s;
			}
		case 5:
			switch (e) {
				case 6:
					return 1;
				case 4:
					return 4;
				default:
					printf("Event is undefined!\n");
					return s;
			}
		default:
		printf("State is undefined!\n");
		return s;
	}
}
