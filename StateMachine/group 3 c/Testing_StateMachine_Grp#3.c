/*
Grp#3
Magid Mostafa
Yasmin Ali
*/
#include <stdio.h>
#include <stdlib.h>

typedef enum {
                S1=1,
                S2=2,
                S3=3,
                S4=4,
                S5=5
            }states;

typedef enum {
                e1=1,
                e2=2,
                e3=3,
                e4=4,
                e5=5,
                e6=6,
                e7=7,
                e8=8,
                e9=9,
                e10=10
            }events;

int readevent(void);

int main()
{
    states state=S1; //initial state= S1
    events event;
    while(1)
    {
        event = readevent();
        switch(state)
        {
            case S1:
            {
                switch(event)
                {
                    case e7:
                    {
                        state = S5;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    case e8:
                    {
                        state = S4;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    default:
                        printf("Invalid event, State is S%d \n\n",state);
                } // end of switch(event) inside S1
            break;
            } // end of case S1

            case S2:
            {
                switch(event)
                {
                    case e2:
                    {
                        state = S1;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    case e10:
                    {
                        state = S3;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    default:
                        printf("Invalid event, state is S%d \n\n",state);
                } // end of switch(event) inside S2
            break;
            } // end of case S2

            case S3:
            {
                switch(event)
                {
                    case e1:
                    {
                        state = S2;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    case e9:
                    {
                        state = S3;
                        printf("State is S%d \n\n",state);
                        break;
                    }
                    default:
                        printf("Invalid event, state is S%d \n\n",state);
                } // end of switch(event) inside S3
            break;
            } // end of case S3

             case S4:
            {
                switch(event)
                {
                    case e3:
                    {
                        state = S2;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    case e5:
                    {
                        state = S5;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    default:
                        printf("Invalid event, state is S%d \n\n",state);
                } // end of switch(event) inside S4
            break;
            } // end of case S4

             case S5:
            {
                switch(event)
                {
                    case e4:
                    {
                        state = S4;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    case e6:
                    {
                        state = S1;
                        printf("New State is S%d \n\n",state);
                        break;
                    }
                    default:
                        printf("Invalid event, state is S%d \n\n",state);
                } // end of switch(event) inside S5
            break;
            } // end of case S5

        break;
        } // end of switch(state)
    } //end of while(1)
    return 0;
}


int readevent(void)
{
    int eventnum;
    char ch;
    printf("Enter an event in numbers (1-10): ");
    while (scanf("%d", &eventnum) !=1)
    {
        while ((ch=getchar()) != '\n')
        {}
        printf("Invalid Input, Enter integer value: ");
    }
    return eventnum;
}


